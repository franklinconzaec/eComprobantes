PrimeFaces.locales['es'] = {
	closeText : 'Cerrar',
	prevText : 'Anterior',
	nextText : 'Siguiente',
	monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
			'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
			'Diciembre' ],
	monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago',
			'Sep', 'Oct', 'Nov', 'Dic' ],
	dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves',
			'Viernes', 'Sábado' ],
	dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
	dayNamesMin : [ 'D', 'L', 'M', 'X', 'J', 'V', 'S' ],
	weekHeader : 'Semana',
	firstDay : 1,
	isRTL : false,
	showMonthAfterYear : false,
	yearSuffix : '',
	timeOnlyTitle : 'Sólo hora',
	timeText : 'Tiempo',
	hourText : 'Hora',
	minuteText : 'Minuto',
	secondText : 'Segundo',
	currentText : 'Fecha actual',
	ampm : false,
	month : 'Mes',
	week : 'Semana',
	day : 'Día',
	allDayText : 'Todo el día'
};

function estiloFila(id, idTabla) {
	var vec = id.split(':');
	var fila = obtenerNumero(vec);
	var tblData = idTabla + '_data';
	document.getElementById(tblData).getElementsByTagName('tr')[fila].classList
			.add("class", 'ui-state-highlight');
}

function sinEstiloFila(id, idTabla) {
	var vec = id.split(':');
	var fila = obtenerNumero(vec);
	var tblData = idTabla + '_data';
	document.getElementById(tblData).getElementsByTagName('tr')[fila].classList
			.remove('ui-state-highlight');
}

function obtenerNumero(vec) {
	for (i = 0; i < vec.length; i++) {
		var a = Number.parseInt(vec[i]);
		if (a.toString() != 'NaN') {
			return a;
		}
	}
	return -1;
}

function teclaAbajoArriba(keyCode, idComponente, idTabla) {
	var tabla = idTabla;
	var vectorIdComponente = idComponente.split(":");
	var idComponenteMitad = vectorIdComponente[0] + ":" + vectorIdComponente[1];
	var idComponenteUltimo = vectorIdComponente[3];
	console.log("tabla - " + tabla);
	console.log("vectorIdComponente - " + vectorIdComponente);
	console.log("idComponenteMitad - " + idComponenteMitad);
	console.log("idComponenteUltimo - " + idComponenteUltimo);

	var posSplit = idComponente.split(":");
	var p = parseInt(posSplit[2]);
	var n = document.getElementById(tabla).childNodes.length;
	if (keyCode == 38) {
		if (p == 0)
			p = n - 1;
		else
			p = p - 1;
		document.getElementById(
				idComponenteMitad + ':' + p + ':' + idComponenteUltimo).focus();
		return false;
	} else if (keyCode == 40) {
		if ((p + 1) == n)
			p = 0;
		else
			p = p + 1;
		document.getElementById(
				idComponenteMitad + ':' + p + ':' + idComponenteUltimo).focus();
		return false;
	}
}
function clickIngresar() {
	$('#btnSubmit').click();
}

function clickCerrar() {
	$('#btnCerrarBusqueda').click();
}

function mostarPanelMostrar(id) {
	$panelPrincipal = $('#' + id + 'panelPrincipal');
	$panelInsertar = $('#' + id + 'panelMostrar');
	$panelPrincipal.slideToggle();
	$panelInsertar.slideToggle();
	return false;
}

function mostarPanelInsertar(id) {
	$panelPrincipal = $('#' + id + 'panelPrincipal');
	$panelInsertar = $('#' + id + 'panelInsertar');
	$panelPrincipal.slideToggle();
	$panelInsertar.slideToggle();
	return false;
}

function mostarPanelImportar(id) {
	$panelPrincipal = $('#' + id + 'panelPrincipal');
	$panelImportar = $('#' + id + 'panelImportar');
	$panelPrincipal.slideToggle();
	$panelImportar.slideToggle();
	return false;
}

function mostarPanelEditar(id) {
	$panelPrincipal = $('#' + id + 'panelPrincipal');
	$panelEditar = $('#' + id + 'panelEditar');
	$panelPrincipal.slideToggle();
	$panelEditar.slideToggle();
	return false;
}

function mostarPanelEliminar(id) {
	$panelPrincipal = $('#' + id + 'panelPrincipal');
	$panelEliminar = $('#' + id + 'panelEliminar');
	$panelPrincipal.slideToggle();
	$panelEliminar.slideToggle();
	return false;
}
function mostarPanelBusqueda(id) {
	$panelBusqueda = $('#' + id + 'panelBusqueda');
	$panelBusqueda.slideToggle();
	return false;
}

function mostarPanel1(id) {
	$panelPrincipal = $('#panelPrincipal');
	$panel = $('#' + id);
	$panelPrincipal.slideToggle();
	$panel.slideToggle();
	return false;
}

function mostarPanel(idPanelPrincipal, idPanel) {
	$panelPrincipal = $('#' + idPanelPrincipal);
	$panel = $('#' + idPanel);
	$panelPrincipal.slideToggle();
	$panel.slideToggle();
	return false;
}

function mostrarPanel(idPanel, idFocus) {
	$panel = $('#' + idPanel);
	$panel.slideDown('slow');
	if (idFocus != '') {
		$fo = $('#' + idFocus);
		$fo.focus();
		setTimeout(function() {
			$fo = $('#' + idFocus);
			$fo.focus();
		}, 300);
	}
	return false;
}

function ocultarPanel(idPanel, idFocus) {
	$panel = $('#' + idPanel);
	$panel.hide();
	if (idFocus != '') {
		$fo = $('#' + idFocus);
		$fo.focus();
		setTimeout(function() {
			$fo = $('#' + idFocus);
			$fo.focus();
		}, 300);
	}
	return false;
}

function comprobarInsertar(xhr, status, args, id) {
	if (!args.validationFailed && args.cerrar) {
		mostarPanelInsertar(id);
	}
}

function comprobarEditar(xhr, status, args, id) {
	if (!args.validationFailed && args.cerrar) {
		mostarPanelEditar(id);
	}
}

function comprobarDialogoPago(xhr, status, args, id) {
	if (!args.error) {
		mostarPanel1(id);
	}
}

function concatenarNuevo() {
	var apellidos = $('#formNuevo\\:apellidos').val();
	var nombres = $('#formNuevo\\:nombres').val();
	$('#formNuevo\\:nombreComercial').val(apellidos + " " + nombres);
}

function concatenarEditar() {
	var apellidos = $('#formEditar\\:apellidos').val();
	var nombres = $('#formEditar\\:nombres').val();
	$('#formEditar\\:nombreComercial').val(apellidos + " " + nombres);
}

function abrirDialogoPago() {
	var tipoPagoVar = $('#txtTipoPago').val();
	if (tipoPagoVar == 1) {
		ocultarPanel('panelTarjeta', '');
		ocultarPanel('panelCheque', '');
		mostrarPanel('panelEfectivo', 'formEfectivo\\:txtCuotaEfectivo');
		$('#formEfectivo\\:txtCuotaEfectivo').val('');
	} else if (tipoPagoVar == 2) {
		ocultarPanel('panelEfectivo', '');
		ocultarPanel('panelCheque', '');
		mostrarPanel('panelTarjeta', 'formTarjeta\\:txtValorTarjeta');
		$('#formTarjeta\\:txtTarjetaCredito').val('');
		$('#formTarjeta\\:txtVaucherCredito').val('');
		$('#formTarjeta\\:txtCuotaCredito').val('');
		PF('somBancoCreditoWV').selectValue(0);
	} else if (tipoPagoVar == 3) {
		ocultarPanel('panelTarjeta', '');
		ocultarPanel('panelEfectivo', '');
		mostrarPanel('panelCheque', 'formCheque\\:txtValorCheque');
		$('#formCheque\\:txtCuentaCheque').val('');
		$('#formCheque\\:txtNumeroCheque').val('');
		$('#formCheque\\:txtCuotaCheque').val('');
		PF('somBancoChequeWV').selectValue(0);
	} else if (tipoPagoVar.toUpperCase() == "PP") {
		guardarFacturaRC();
	}
}

function esq() {
	ocultarPanel('panelFormaPago');
	ocultarPanel('panelPtoEmision');
	ocultarPanel('panelVendedor');
	ocultarPanel('panelBuscarProducto');
	ocultarPanel('panelCantidadProducto');
	mostrarPanel('panelTabla');
	mostrarPanel('panelDatos');
}

function nuevaFactura() {
	nuevaFacturaRC();
}

function comprobarDatosFactura() {
	comprobarRC();
}

var producto;

function focusProducto() {
	if (producto != undefined) {
		producto.focus();
		setTimeout(function() {
			producto.focus();
		}, 300);
	}
}

function obtenerFilaProducto(registroProducto) {
	console.log(registroProducto);
	producto = document.getElementById(registroProducto);
	console.log(producto);
}

function getStorage() {
	var storage;
	try {
		if (localStorage.getItem) {
			storage = localStorage;
		}
	} catch (e) {
		storage = {};
	}
	return storage;
}

var guardarProductos = function(productos) {
	storage = getStorage();
	storage.productos = JSON.stringify(productos);
}

var getProducto = function(codigo) {
	storage = getStorage();
	var productos = JSON.parse(storage.productos);
	var producto = ''
	productos.some(function(pro) {
		if (pro.codigo == codigo) {
			producto = pro;
			return true;
		}
	});
	return producto;
}

var cambiarEstado = function(componente, codigo) {
	var i = parseInt(componente.split(':')[2]);
	var estado = $(
			'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:estado_label')
			.text();
	var $impuesto = $('#formDataTable\\:tablaDetallePedido\\:' + i
			+ '\\:impuesto');
	if (estado === 'NORMAL') {
		$impuesto.text(getProducto(codigo).impuesto);
	} else if (estado === 'PROMOCIÓN')
		$impuesto.text('P');
	else if (estado === 'CAMBIO')
		$impuesto.text('C');
	else if (estado === 'PROMOCIÓN INTERNA')
		$impuesto.text('PI');

	if ($impuesto.text() === '' || $impuesto.text() === 'I')
		calcularImporte(componente);
	else
		$('#formDataTable\\:tablaDetallePedido\\:' + i + '\\:importe').text(
				'0.000000');
	calcularCantidadFinal();
}

var cambiarTipoPrecio = function(componente, codigo) {
	var i = parseInt(componente.split(':')[2]);
	var tipoPrecio = $(
			'#formDataTable\\:tablaDetallePedido\\:' + i
					+ '\\:tipoPrecio_label').text();
	var $impuesto = $('#formDataTable\\:tablaDetallePedido\\:' + i
			+ '\\:impuesto');
	var producto = getProducto(codigo);
	var valor = producto.precio;
	producto.tipoPrecios.some(function(tpp) {
		if (tpp.nombre == tipoPrecio) {
			valor = tpp.porcentajePrecioFijo ? completarDecimales(
					redondeo(producto.precio
							+ ((producto.precio * tpp.valor) / 100), 6), 6)
					: tpp.valor;
			return true;
		}
	});
	$('#formDataTable\\:tablaDetallePedido\\:' + i + '\\:precioUnitario').text(
			valor);
	if ($impuesto.text() === '' || $impuesto.text() === 'I') {
		calcularImporte(componente);
		calcularCantidadFinal();
	}
}

var calcularImporte = function(componente) {
	var i = parseInt(componente.split(':')[2]);
	var cantidad = Number($(
			'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:cantidad').val());
	var precioUnitario = Number($(
			'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:precioUnitario')
			.text());
	var impuesto = $(
			'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:impuesto')
			.text();
	var importe = completarDecimales(redondeo(cantidad * precioUnitario, 6), 6);
	if (impuesto !== '' && impuesto !== 'I')
		importe = '0.000000';
	$('#formDataTable\\:tablaDetallePedido\\:' + i + '\\:importe')
			.text(importe);
	calcularCantidadFinal();
}

var calcularCantidadFinal = function() {
	var size = $('#formDataTable\\:tablaDetallePedido_data').children('tr')
			.size();
	var subTotalIva = 0.00;
	var subTotal0 = 0.00;
	var subTotal = 0.00;
	var descuento = 0.00;
	var iva = 0.00;
	var total = 0.00;
	for (i = 0; i < size; i++) {
		var impuesto = $(
				'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:impuesto')
				.text();
		var importe = Number($(
				'#formDataTable\\:tablaDetallePedido\\:' + i + '\\:importe')
				.text());
		if (impuesto === 'I')
			subTotalIva = subTotalIva + importe;
		else if (impuesto === '')
			subTotal0 = subTotal0 + importe;
		else {
			var cantidad = Number($(
					'#formDataTable\\:tablaDetallePedido\\:' + i
							+ '\\:cantidad').val());
			var precioUnitario = Number($(
					'#formDataTable\\:tablaDetallePedido\\:' + i
							+ '\\:precioUnitario').text());
			descuento = descuento + (cantidad * precioUnitario);
		}
	}
	subTotal = subTotal0 + subTotalIva;
	iva = subTotalIva * 0.14;
	total = subTotal + iva;

	redondear = function(val) {
		return completarDecimales(redondeo(val, 2), 2);
	}

	$('#formTotales\\:subTotalIva').text(redondear(subTotalIva));
	$('#formTotales\\:subTotal0').text(redondear(subTotal0));
	$('#formTotales\\:subTotal').text(redondear(subTotal));
	$('#formTotales\\:descuento').text(redondear(descuento));
	$('#formTotales\\:iva').text(redondear(iva));
	$('#formTotales\\:total').text(redondear(total));
}

var completarDecimales = function(val, dec) {
	val = String(val);
	var split = val.split('.')
	var entero = split[0];
	var decimales = split[1];
	if (split.length === 1) {
		decimales = '0'
	}
	for (i = decimales.length; i < dec; i++) {
		decimales += '0';
	}
	return entero + '.' + decimales;
}

function redondeo(numero, decimales) {
	var flotante = parseFloat(numero);
	return Math.round(flotante * Math.pow(10, decimales))
			/ Math.pow(10, decimales);
}
