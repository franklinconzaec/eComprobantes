package ec.com.redepronik.eComprobantes.service;

import java.io.InputStream;
import java.util.List;

import ec.com.redepronik.eComprobantes.entity.Anio;
import ec.com.redepronik.eComprobantes.entity.DocumentoElectronico;
import ec.com.redepronik.eComprobantes.entity.Mes;

public interface DocumentosElectronicosService {

	public List<DocumentoElectronico> getDocumentosAutorizados(Mes mes, Anio anio);

	public InputStream getFileXML(String nombreDocumento);

	public InputStream getFileRIDE(String nombreDocumento);

	public void getDocumentosPorPersona();

}
