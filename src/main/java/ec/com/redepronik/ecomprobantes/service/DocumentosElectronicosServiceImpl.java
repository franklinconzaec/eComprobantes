package ec.com.redepronik.eComprobantes.service;

import static ec.com.redepronik.eComprobantes.utils.Utils.getArchivo;
import static ec.com.redepronik.eComprobantes.utils.Utils.getArchivosPorPersona;
import static ec.com.redepronik.eComprobantes.utils.Utils.subString;
import static ec.com.redepronik.eComprobantes.utils.Utils.tipoDocumento;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ec.com.redepronik.eComprobantes.entity.Anio;
import ec.com.redepronik.eComprobantes.entity.DocumentoElectronico;
import ec.com.redepronik.eComprobantes.entity.Mes;

@Service
public class DocumentosElectronicosServiceImpl implements DocumentosElectronicosService {

	private List<String> listArchivos;

	public List<DocumentoElectronico> getDocumentosAutorizados(Mes mes, Anio anio) {
		String fec = mes.getId() + anio.getId();
		List<DocumentoElectronico> list = new ArrayList<DocumentoElectronico>();
		for (String s : listArchivos) {
			if (fec.compareToIgnoreCase(s.split("-")[1].substring(2, 8)) == 0)
				list.add(new DocumentoElectronico(s, tipoDocumento(s)));
		}

		return list;
	}

	public InputStream getFileXML(String nombreDocumento) {
		return getArchivo(nombreDocumento);
	}

	public InputStream getFileRIDE(String nombreDocumento) {
		return getArchivo(nombreDocumento);
	}

	public void getDocumentosPorPersona() {
		System.out.println("ced: " + SecurityContextHolder.getContext().getAuthentication().getName());
		listArchivos = null;
		System.out.println("1 " + listArchivos);
		listArchivos = new ArrayList<String>();
		System.out.println("2 " + listArchivos);
		for (String s : getArchivosPorPersona(SecurityContextHolder.getContext().getAuthentication().getName()))
			if (s.endsWith(".xml"))
				listArchivos.add(subString(s, ".xml"));
	}
}