package ec.com.redepronik.eComprobantes.entity;

import java.io.Serializable;

public class DocumentoElectronico implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombreDocumento;
	private String tipoDocumento;

	public DocumentoElectronico() {
	}

	public DocumentoElectronico(String nombreDocumento, String tipoDocumento) {
		this.nombreDocumento = nombreDocumento;
		this.tipoDocumento = tipoDocumento;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

}
