package ec.com.redepronik.eComprobantes.utils;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class Utils {

	private static AmazonS3File amazonS3File = new AmazonS3File(desencriptar("DNLDMTU5SQN866]EY[9T"),
			desencriptar("PykI3r.Osh{S7[huqUEx]<6GhhVVkez|Pd|2wwqW"), desencriptar("vxshuqlvkr"));

	public static String desencriptar(String palabra) {
		String aux = "";
		for (int i = 0; i < palabra.length(); i++)
			aux += (char) (palabra.charAt(i) - 3);
		return aux;
	}

	public static InputStream getArchivo(String ruta) {
		return amazonS3File.getObject(ruta);
	}

	public static List<String> getArchivosPorPersona(String cedula) {
		return amazonS3File.listArchivos(cedula);
	}

	public static String invertirPalabra(String palabra) {
		return new StringBuilder(palabra).reverse().toString();
	}

	public static void presentaMensaje(Severity severity, String mensaje) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, "MENSAJE DEL SISTEMA", mensaje));
	}

	public static String subString(String palabra, String subS) {
		palabra = invertirPalabra(palabra);
		return invertirPalabra(palabra.substring(subS.length(), palabra.length()));
	}

	public static String tipoDocumento(String claveAcceso) {
		switch (claveAcceso.split("-")[1].substring(8, 10)) {
		case "01":
			return "FACTURA";
		case "04":
			return "NOTA DE CRÉDITO";
		case "05":
			return "NOTA DE DÉBITO";
		case "06":
			return "GUÍA DE REMISIÓN";
		case "07":
			return "COMPROBANTE DE RETENCIÓN";
		default:
			return "";
		}
	}

	public static String fechaFormatoString(Date fecha) {
		return new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	}

}
