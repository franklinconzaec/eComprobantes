package ec.com.redepronik.eComprobantes.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("ec.com.redepronik")
@Import({ PersistenceConfig.class, SecurityConfig.class })
public class AppConfig {

}
