package ec.com.redepronik.eComprobantes.controller;

import static ec.com.redepronik.eComprobantes.utils.Utils.fechaFormatoString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import ec.com.redepronik.eComprobantes.entity.Anio;
import ec.com.redepronik.eComprobantes.entity.DocumentoElectronico;
import ec.com.redepronik.eComprobantes.entity.Mes;
import ec.com.redepronik.eComprobantes.service.DocumentosElectronicosService;

@Controller
@Scope("session")
public class DocumentosRecibidosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private DocumentosElectronicosService documentosElectronicosService;

	private List<DocumentoElectronico> listaDocumentosAutorizados;
	private StreamedContent documento;
	private Mes mes;
	private Anio anio;

	public DocumentosRecibidosBean() {
	}

	public Mes[] getListaMes() {
		return Mes.values();
	}

	public Anio[] getListaAnio() {
		return Anio.values();
	}

	public void descargarXML(DocumentoElectronico documentoElectronico) {
		String nomDocmento = documentoElectronico.getNombreDocumento() + ".xml";
		documento = new DefaultStreamedContent(documentosElectronicosService.getFileRIDE(nomDocmento), "text/xml",
				nomDocmento);
	}

	public void descargarRIDE(DocumentoElectronico documentoElectronico) {
		String nomDocmento = documentoElectronico.getNombreDocumento() + ".pdf";
		documento = new DefaultStreamedContent(documentosElectronicosService.getFileRIDE(nomDocmento),
				"application/pdf", nomDocmento);
	}

	public StreamedContent getDocumento() {
		return documento;
	}

	public List<DocumentoElectronico> getListaDocumentosAutorizados() {
		return listaDocumentosAutorizados;
	}

	@PostConstruct
	public void init() {
		mes = Mes.obtenerPorId(fechaFormatoString(new Date()).substring(3, 5));
		anio = Anio.obtenerPorId(fechaFormatoString(new Date()).substring(6, 10));
		documentosElectronicosService.getDocumentosPorPersona();
	}

	public void obtener() {
		listaDocumentosAutorizados = documentosElectronicosService.getDocumentosAutorizados(mes, anio);
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public Anio getAnio() {
		return anio;
	}

	public void setAnio(Anio anio) {
		this.anio = anio;
	}

	public void setDocumento(StreamedContent documento) {
		this.documento = documento;
	}

	public void setListaDocumentosAutorizados(List<DocumentoElectronico> listaDocumentosAutorizados) {
		this.listaDocumentosAutorizados = listaDocumentosAutorizados;
	}
}
